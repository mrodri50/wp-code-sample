//WP query and loop
$prefix = 'manny';
add_shortcode($prefix.'_post_list', 'list_posts_sc');
function list_posts_sc($atts) {
  extract(shortcode_atts(array(
      'post_type' => 'team',
      'posts_per_page' => 6,
      'paging' => 1,
      'featured_image' => 1,
      'excerpt_length' => 45,
      'order' => 'DESC',
      'orderby' => 'date',
      'wrapper_class' => null,
      'item_class' => null,
      'content_class' => null,
      //'image_size' => array( 200, 200),
      'image_size' => 'full',
      'image_class' => null
  ), $atts));
  $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
  $args = array(
      'post_type' => $post_type,
      'posts_per_page' => $posts_per_page,
      'paged' => $paged,
      'order' => $order,
      'orderby' => $orderby
  );
  $query = new WP_Query($args);
  $output = '';
  
  if($query->have_posts()):

      $output .= '<section class="team-list-wrapper cf '.$wrapper_class.'">';
      
      // THE LOOP
      while ($query->have_posts()) : $query->the_post();
      
      // Vars from ACF team member    
      $bio = get_field('team_bio');
      $name = get_field('team_name');
      $title = get_field('team_title');  
      $social_media_icons = [
          get_field('linkedin_link'), 
          get_field('twitter_link'), 
          get_field('facebook_link')
      ];
      //$social_media_icons = get_field('social_media_icons');

          $output .= '<article class="team-item '.$item_class.'">';
              // FEATURED IMAGE
              if(has_post_thumbnail() && $featured_image):
                  global $post;
                  $output .= '<div class="team-background" style="background-image: url('. get_the_post_thumbnail_url() .')">';
                    $output .= '<div class="team-overlay">';
                        // TEAM MEMBER NAME
                        if($name):    
                            $output .= '<h4 class="team-name">'. $name .'</h4>';
                        endif;
                        // TEAM MEMBER TITLE        
                        if($title):    
                            $output .= '<p class="team-title">'. $title .'</p>';
                        endif;
                    $output .= '</div>';
                  $output .= '</div>';     

              endif;
              $output .= '<div class="team-content '.$content_class.'">';
                // TEAM MEMBER SOCIAL        
                //if($social_media_icons[0] !=null):
                    $output .= '<div class="team-social">';
                    if($social_media_icons[0] !=null):                           
                        $output .= '<a href=" '.$social_media_icons[0].' " target="_blank"><i class="fa fa-linkedin"></i> </a>';
                    endif;
                    if($social_media_icons[1] !=null):     
                        $output .= '<a href=" '.$social_media_icons[1].' " target="_blank"><i class="fa fa-twitter"></i> </a>';
                    endif;
                    if($social_media_icons[2] !=null):
                        $output .= '<a href=" '.$social_media_icons[2].' " target="_blank"><i class="fa fa-facebook-official"></i> </a>';
                    endif;    
                    $output .= '</div>';                
                //endif;
                // TEAM MEMBER BIO        
                if($bio):    
                    $output .= '<p class="team-bio">'. $bio .'</p>';
                endif;     
                  
              $output .= '</div>';
          $output .= '</article>';
      endwhile;
      // END THE LOOP
      
      $output .= '</section>';
      
      // PAGINATION
      if($paging):
          global $wp_query;
          $big = 999999999;
          $pagination = paginate_links( array(
              'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
              'format' => '?paged=%#%',
              'current' => max( 1, get_query_var('paged') ),
              'total' => $query->max_num_pages
          ) );
          if($pagination):
              $output .= '<div class="pagination">';
                  $output .= $pagination;
              $output .= '</div>';
          endif;
      endif;
      
      // RESTORE ORIGINAL POST DATA
      wp_reset_postdata();
  
  else:
      $output .= '<p>Sorry, no posts matched your criteria.</p>';
  endif;
  
  return $output;
}