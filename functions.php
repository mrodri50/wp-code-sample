<?php
/**
 * Twenty Seventeen functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 */

/**
 * This is the coded added to the functions file to make add the custom shortcodes and my custom post type calle "teams"
 */



/*
* BEGINING Manny CUSTOM CODE
*/



// Custom shortcodes

require get_parent_theme_file_path( '/inc/shortcodes.php' );


// Custom post type team
function team_posttype() {
 
    register_post_type( 'team',
    // CPT Options
        array(
            'labels' => array(
                'name' => __( 'Team' ),
                'singular_name' => __( 'Team' )
            ),
            'public' 		=> true,
            'has_archive' 	=> true,
			'rewrite' 		=> array('slug' => 'team'),
			'supports' 		=> array( 'title', 'thumbnail', 'custom-fields' ), //it will show post title, and feature image
        )
    );
}
add_action( 'init', 'team_posttype' );

// enqueue custom css and JS
function wpdocs_theme_name_scripts() {
	wp_enqueue_style( 'custom', get_template_directory_uri() . '/assets/css/custom.css', array() );
	wp_enqueue_style('font-awesome', 'https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css'); 
    wp_enqueue_script( 'custom', get_template_directory_uri() . '/assets/js/custom.js', array(), '1.0.0', true );
}
add_action( 'wp_enqueue_scripts', 'wpdocs_theme_name_scripts' );
