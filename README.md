# WP Code Sample

This is a simple short-code that generate a WP query to loop trough a custom post type &#34;team&#34; and display the headshots and a small bio from the post using ACF to gather the data. 
It uses simple jQuery for the slider animation and it&#39;s fully responsive using bootstrap and flexbox elements. The scss is compiled using sass and include some simple examples of @mixin usage. There was not need of adding npm and gulp for this simple task.

Built using;
CMS: WordPress 4.9.8.
Theme: twentyseventeen.
Tools and libraries:
Sass (compile css)
Jquery
Php
WP API
Advance Custom Fields

wp-admin access request to manuel.rodriguez.web@gmail.com
