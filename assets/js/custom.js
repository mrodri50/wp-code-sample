jQuery(document).ready(function($) {

    /***********************
    Write your script below!
    ************************/
   
    $('.team-overlay').on('click', function(){
        $('.team-content').removeClass('active').slideUp('slow');
        $('.team-overlay').removeClass('overlay-active');
        $(this).parent().next('.team-content').stop().toggleClass('active').slideToggle('slow');
        $(this).toggleClass('overlay-active');
    });
    
});